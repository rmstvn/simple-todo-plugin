export class TodoList {
  constructor(node) {
    this.node = document.querySelector(node);

    this.addItem = this.addItem.bind(this);
    this.deleteAll = this.deleteAll.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.holdItem = this.holdItem.bind(this);
    this.renderTodos = this.renderTodos.bind(this);
    this.checkHolded = this.checkHolded.bind(this);
    this.renderModalMessage = this.renderModalMessage.bind(this);
    this.renderHeading = this.renderHeading.bind(this);

    this.todos = JSON.parse(localStorage.getItem("todos"))
      ? JSON.parse(localStorage.getItem("todos"))
      : [];

    this.init();
  }

  init() {
    const basicMarkup = `
    <div class="todo">
    <h1 class="todo__heading"> </h1>

        <form class="todo__form">
            <input type="text" name="todo-item" class="text-input todo__input" maxlength="40" />
            <button class="btn todo__submit-input"><i class="material-icons">
add_box
</i></button>
        </form>
        <ul class="todo__list">
        </ul>
        <button class="btn todo__delete-all">Delete All</button>
    </div>
    `;

    this.node.insertAdjacentHTML("afterbegin", basicMarkup);

    window.addEventListener("load", e => {
      this.renderHeading();
      if (this.todos.length > 0) {
        this.renderTodos();
        this.checkHolded();
      } else localStorage.setItem("todos", JSON.stringify([]));
    });

    this.node.addEventListener("submit", e => {
      if (e.target.className.includes("todo__form")) {
        this.addItem(e);
      }
    });

    this.node.addEventListener("click", e => {
      if (e.target.className.includes("todo__delete-all")) {
        this.deleteAll(e);
      }
      if (e.target.className.includes("todo__delete")) {
        this.deleteItem(e);
      }
    });

    this.node.addEventListener("change", e => {
      if (e.target.className.includes("todo__hold-input")) {
        this.holdItem(e);
      }
    });
  }

  addItem(e) {
    e.preventDefault();

    const input = this.node.querySelector(".todo__input");

    if (input.value === "") {
      return;
    }

    if (this.todos.length > 3) {
      this.renderModalMessage("You can add only 4 things to do");
      return;
    }

    this.todos.push({
      title: input.value,
      isHold: false,
      id: Date.now()
    });

    localStorage.setItem("todos", JSON.stringify(this.todos));
    input.value = "";

    this.renderTodos();
    this.checkHolded();
    this.renderHeading();
  }

  holdItem(e) {
    const todoItem = e.target.closest(".todo__item");
    const index = this.todos.findIndex(todo => {
      return +todo.id === parseInt(todoItem.dataset.id);
    });
    this.todos[index].isHold = e.target.checked;
    const deleteBtn = todoItem.querySelector(".todo__delete");
    if (e.target.checked) {
      todoItem.classList.add("untouchable");
      deleteBtn.setAttribute("disabled", "disabled");
    } else {
      todoItem.classList.remove("untouchable");
      deleteBtn.removeAttribute("disabled");
    }
    localStorage.setItem("todos", JSON.stringify(this.todos));
  }

  deleteItem(e) {
    e.preventDefault();
    console.log(e);
    const todoItem = e.target.closest(".todo__item");
    const index = this.todos.findIndex(todo => {
      return +todo.id === parseInt(todoItem.dataset.id);
    });

    this.todos.splice(index, 1);
    localStorage.setItem("todos", JSON.stringify(this.todos));
    this.renderTodos();
    this.checkHolded();
    this.renderHeading();
  }

  deleteAll(e) {
    e.preventDefault();
    const checked = this.checkHolded();
    localStorage.removeItem("todos");
    localStorage.setItem("todos", JSON.stringify(checked));
    this.todos = checked;
    this.renderTodos();
    this.checkHolded();
    this.renderHeading();
  }

  checkHolded() {
    const checked = this.todos.filter(todo => todo.isHold === true);
    checked.forEach(todo => {
      const node = this.node.querySelector(`[data-id='${todo.id}']`);
      node.classList.add("untouchable");
      node
        .querySelector(".todo__hold-input")
        .setAttribute("checked", "checked");
      node.querySelector(".todo__delete").setAttribute("disabled", "disabled");
    });
    return checked;
  }

  renderHeading() {
    this.node.querySelector(".todo__heading").innerHTML = `
              ${
                this.todos.length > 0
                  ? `You have ${this.todos.length} ${
                      this.todos.length > 1 ? "tasks" : "task"
                    }`
                  : "You have nothing to do"
              }`;
  }

  renderTodos() {
    this.node.querySelector(".todo__list").innerHTML = `
     ${
       this.todos.length > 0
         ? this.todos
             .map(
               todo => `
            <li class="todo__item" data-id=${todo.id} dragable="true">
               <span class="todo__title">${todo.title}</span>
               <span class="todo__item-wrapper">
                   <label class="todo__hold-wrapper">
                       <input type="checkbox" class="todo__hold-input" />
               
                       <span class="todo__hold">
                           <i class="material-icons">
block
</i>
                       </span>
                   </label>
                   <button class="btn todo__delete"><i class="material-icons">
clear
</i></button>
               </span>
           </li>
       `
             )
             .join("")
         : ""
     }
     `;
  }

  renderModalMessage(msg) {
    const message = `
        <div class="modal-message">${msg}</div>
        `;
    this.node.insertAdjacentHTML("beforeend", message);
    setTimeout(() => {
      this.node.querySelector(".modal-message").remove();
    }, 4000);
  }
}
